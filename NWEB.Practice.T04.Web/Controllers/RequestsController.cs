﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NWEB.Practice.T04.Core;
using NWEB.Practice.T04.Core.Data;
using NWEB.Practice.T04.DataAccessLayer.Infrastructures;

namespace NWEB.Practice.T04.Web.Controllers
{
    public class RequestsController : Controller
    {
        private readonly IUnitOfWork  _unitOfWork;

        public RequestsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Authorize(Roles = $"{SD.ROLE_USER},{SD.ROLE_ADMIN}")]
        public async Task<IActionResult> Index()
        {
              return _unitOfWork.DbContext.Requests != null ? 
                          View(await _unitOfWork.DbContext.Requests.ToListAsync()) :
                          Problem("Entity set 'ClassDbContext.Requests'  is null.");
        }

        [Authorize(Roles = $"{SD.ROLE_USER},{SD.ROLE_ADMIN}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _unitOfWork.DbContext.Requests == null)
            {
                return NotFound();
            }

            var request = await _unitOfWork.DbContext.Requests
                .FirstOrDefaultAsync(m => m.Id == id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        [Authorize(Roles = $"{SD.ROLE_USER}")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,StudentName,ClassName,Title,LeaveDate,LeaveType,Reason,Status")] Request request)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbContext.Add(request);
                await _unitOfWork.DbContext.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(request);
        }

        [Authorize(Roles = $"{SD.ROLE_USER}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _unitOfWork.DbContext.Requests == null)
            {
                return NotFound();
            }

            var request = await _unitOfWork.DbContext.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            return View(request);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,StudentName,ClassName,Title,LeaveDate,LeaveType,Reason,Status")] Request request)
        {
            if (id != request.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _unitOfWork.DbContext.Update(request);
                    await _unitOfWork.DbContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RequestExists(request.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(request);
        }
        [Authorize(Roles = $"{SD.ROLE_USER}")]
        public async Task<IActionResult> Cancel(int? id)
        {
            if (id == null || _unitOfWork.DbContext.Requests == null)
            {
                return NotFound();
            }

            var request = await _unitOfWork.DbContext.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            return View(request);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Cancel(int id, [Bind("Id,StudentName,ClassName,Title,LeaveDate,LeaveType,Reason,Status")] Request request)
        {
            if (id != request.Id)
            {
                return NotFound();
            }
                    _unitOfWork.DbContext.Update(request);
                    await _unitOfWork.DbContext.SaveChangesAsync();
                
                
                return RedirectToAction(nameof(Index));
 
        }
        [Authorize(Roles = $"{SD.ROLE_ADMIN}")]
        public async Task<IActionResult> Approve(int? id)
        {
            if (id == null || _unitOfWork.DbContext.Requests == null)
            {
                return NotFound();
            }

            var request = await _unitOfWork.DbContext.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            return View(request);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Approve(int id, [Bind("Id,StudentName,ClassName,Title,LeaveDate,LeaveType,Reason,Status")] Request request)
        {
            if (id != request.Id)
            {
                return NotFound();
            }



            _unitOfWork.DbContext.Update(request);
            await _unitOfWork.DbContext.SaveChangesAsync();


            return RedirectToAction(nameof(Index));

        }
        [Authorize(Roles = $"{SD.ROLE_ADMIN}")]
        public async Task<IActionResult> Reject(int? id)
        {
            if (id == null || _unitOfWork.DbContext.Requests == null)
            {
                return NotFound();
            }

            var request = await _unitOfWork.DbContext.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            return View(request);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Reject(int id, [Bind("Id,StudentName,ClassName,Title,LeaveDate,LeaveType,Reason,Status")] Request request)
        {
            if (id != request.Id)
            {
                return NotFound();
            }
            _unitOfWork.DbContext.Update(request);
            await _unitOfWork.DbContext.SaveChangesAsync();


            return RedirectToAction(nameof(Index));

        }
        [Authorize(Roles = $"{SD.ROLE_USER},{SD.ROLE_ADMIN}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _unitOfWork.DbContext.Requests == null)
            {
                return NotFound();
            }

            var request = await _unitOfWork.DbContext.Requests
                .FirstOrDefaultAsync(m => m.Id == id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_unitOfWork.DbContext.Requests == null)
            {
                return Problem("Entity set 'ClassDbContext.Requests'  is null.");
            }
            var request = await _unitOfWork.DbContext.Requests.FindAsync(id);
            if (request != null)
            {
                _unitOfWork.DbContext.Requests.Remove(request);
            }
            
            await _unitOfWork.DbContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RequestExists(int id)
        {
          return (_unitOfWork.DbContext.Requests?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
