﻿using NWEB.Practice.T04.Core.Data;
using NWEB.Practice.T04.DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.DataAccessLayer.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ClassDbContext dbContext;
        private IRequestRepository requestRepository;

        public UnitOfWork(ClassDbContext context)
        {
            this.dbContext = context;
        }
        public IRequestRepository RequestRepository
        {
            get
            {
                if (this.requestRepository == null)
                {
                    this.requestRepository = new RequestRepository(this.dbContext);
                }
                return this.requestRepository;
            }
        }

        public ClassDbContext DbContext => this.dbContext;

       

        public void Dispose()
        {
            this.dbContext.Dispose();
        }

        public int SaveChange()
        {
            return this.dbContext.SaveChanges();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await this.dbContext.SaveChangesAsync();
        }
    }
}
