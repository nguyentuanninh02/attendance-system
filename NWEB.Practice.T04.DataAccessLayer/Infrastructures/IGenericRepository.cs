﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.DataAccessLayer.Infrastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(params object[] ids);
        TEntity Find(object id);
        IEnumerable<TEntity> GetAll();
    }
}
