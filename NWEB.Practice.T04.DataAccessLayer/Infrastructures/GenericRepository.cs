﻿using Microsoft.EntityFrameworkCore;
using NWEB.Practice.T04.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.DataAccessLayer.Infrastructures
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class

    {
        protected readonly ClassDbContext dbContext;
        protected DbSet<TEntity> entities;

        public GenericRepository(ClassDbContext dbContext)
        {
            this.dbContext = dbContext;
            entities = dbContext.Set<TEntity>();
        }
        public void Add(TEntity entity)
        {
            entities.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            if (dbContext.Entry(entity).State == EntityState.Detached)
            {
                entities.Attach(entity);
            }
            entities.Remove(entity);
        }

        public void Delete(params object[] ids)
        {
            var entity = entities.Find(ids);
            if(entity == null)
                throw new ArgumentException($"{string.Join(";", ids)} not exist in the {typeof(TEntity).Name} table");
            entities.Remove(entity);
        }

        public TEntity Find(object id)
        {
            return entities.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return entities;
        }

        public void Update(TEntity entity)
        {
            entities.Update(entity);
        }
    }

}
