﻿using NWEB.Practice.T04.Core.Data;
using NWEB.Practice.T04.DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.DataAccessLayer.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        IRequestRepository RequestRepository { get; }
        ClassDbContext DbContext { get; }
        int SaveChange();

        Task<int> SaveChangeAsync();

    }
}
