﻿using NWEB.Practice.T04.Core;
using NWEB.Practice.T04.Core.Data;
using NWEB.Practice.T04.DataAccessLayer.Infrastructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.DataAccessLayer.Repositories
{
    public class RequestRepository : GenericRepository<Request>, IRequestRepository
    {
        public RequestRepository(ClassDbContext dbContext) : base(dbContext)
        {
        }      
    }
}
