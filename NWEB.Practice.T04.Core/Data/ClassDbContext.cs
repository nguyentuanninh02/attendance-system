﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core.Data
{
    public class ClassDbContext : IdentityDbContext
    {
        public ClassDbContext()
        {
        }
        public ClassDbContext(DbContextOptions<ClassDbContext> options) : base(options)
        {
        }

     
        public DbSet<Request> Requests { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-1MDGVVK\SQLEXPRESS;Initial Catalog=JustDBContext;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Request>(entity =>
            {                
                entity.HasKey(c => c.Id);
                entity.Property(c => c.StudentName).HasMaxLength(100);
                entity.Property(c => c.ClassName).HasMaxLength(100);
                entity.Property(c => c.Title).HasColumnType("nvarchar");
               
            });
            

            modelBuilder.Seed();
            
        }
    }
}

