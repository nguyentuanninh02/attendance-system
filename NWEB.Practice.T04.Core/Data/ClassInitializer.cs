﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core.Data
{
    public static class ClassInitializer
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Request>().HasData(new List<Request>() {
                new Request()
                {
                    Id = 1,
                    StudentName = "Hoàng Trọng Hiếu",
                    ClassName = "KS05",
                    Title = "Xin nghỉ ngày. . .",
                    LeaveDate = new DateTime(2023, 09, 10),
                    LeaveType = Enum.LeaveType.LateComing,
                    Reason = "Bị ốm .. . ",
                    Status = Enum.Status.Approved
                },

                new Request()
                {
                    Id = 2,
                    StudentName = "Đậu Đình Mạnh",
                    ClassName = "KS05",
                    Title = "Xin nghỉ ngày. . .",
                    LeaveDate = new DateTime(2023, 10, 20),
                    LeaveType = Enum.LeaveType.LeaveAHaftOfDay,
                    Reason = "Đón người yêu đi chơi ",
                    Status = Enum.Status.Rejected
                },

                new Request()
                {
                    Id = 3,
                    StudentName = "Đỗ Nhật Linh",
                    ClassName = "KS05",
                    Title = "Xin nghỉ ngày . . .",
                    LeaveDate = new DateTime(2023, 11, 06),
                    LeaveType = Enum.LeaveType.EarlyLeave,
                    Reason = "Ngủ quên ",
                    Status = Enum.Status.Cancelled
                },

                new Request()
                {
                    Id = 4,
                    StudentName = "Tuấn Ninh",
                    ClassName = "KS05",
                    Title = "Xin nghỉ ngày. . .",
                    LeaveDate = new DateTime(2023, 10, 20),
                    LeaveType = Enum.LeaveType.LateComing,
                    Reason = "Bị ốm . . .",
                    Status = Enum.Status.Submitted
                },

                new Request()
                {
                    Id = 5,
                    StudentName = "Nguyễn Xuân Hậu",
                    ClassName = "KS05",
                    Title = "Xin nghỉ ngày . . .",
                    LeaveDate = new DateTime(2023, 12, 21),
                    LeaveType = Enum.LeaveType.LeaveOneDay,
                    Reason = "Tổ chức sinh nhật",
                    Status = Enum.Status.Submitted
                },
                new Request()
                {
                    Id = 6,
                    StudentName = "Nguyễn Văn Khải",
                    ClassName = "KS05",
                    Title = "Xin nghỉ ngày . . .",
                    LeaveDate = new DateTime(2023, 12, 21),
                    LeaveType = Enum.LeaveType.LeaveOneDay,
                    Reason = "Tổ chức sinh nhật",
                    Status = Enum.Status.Submitted
                }
            });
        }
    }
}
