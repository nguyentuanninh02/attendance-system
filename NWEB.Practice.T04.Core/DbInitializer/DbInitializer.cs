﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NWEB.Practice.T04.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core.DbInitializer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ClassDbContext _db;

        public DbInitializer(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ClassDbContext db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _db = db;
        }


        public void Initialize()
        {
            //migrations if they are not applied
            try
            {
                if (_db.Database.GetPendingMigrations().Count() > 0)
                {
                    _db.Database.Migrate();
                }
            }
            catch (Exception ex)
            {

            }


            //create roles if they are not created
            if (!_roleManager.RoleExistsAsync(SD.ROLE_ADMIN).GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole(SD.ROLE_ADMIN)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(SD.ROLE_USER)).GetAwaiter().GetResult();


                //if roles are not created, then we will create admin user as well
                //Admin class account
                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "admin@gmail.com",
                    Email = "admin@gmail.com",
                    Name = "Admin_Class",
                    PhoneNumber = "12356789",
                   
                }, "Admin123*").GetAwaiter().GetResult();

                ApplicationUser user = _db.ApplicationUsers.FirstOrDefault(u => u.Email == "admin@gmail.com");

                _userManager.AddToRoleAsync(user, SD.ROLE_ADMIN).GetAwaiter().GetResult();
                //student account
                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "student@gmail.com",
                    Email = "student@gmail.com",
                    Name = "student",
                    PhoneNumber = "0144785236555",
                   
                }, "Student123*").GetAwaiter().GetResult();

                ApplicationUser student = _db.ApplicationUsers.FirstOrDefault(u => u.Email == "student@gmail.com");

                _userManager.AddToRoleAsync(student, SD.ROLE_USER).GetAwaiter().GetResult();
            }
            return;
        }
    }
}
