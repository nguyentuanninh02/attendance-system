﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core.DbInitializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
