﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
       
    }
}
