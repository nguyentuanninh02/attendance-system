﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core.Enum
{
    public enum LeaveType
    {
        LateComing,
        EarlyLeave,
        LeaveAHaftOfDay,
        LeaveOneDay

    }
}
