﻿using NWEB.Practice.T04.Core.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T04.Core
{
    public class Request
    {
        public int Id { get; set; }
        
        [Display(Name = "Student Name")]
        [Required(ErrorMessage = "Student Name is required!")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Name must be more than 3 characters and less than 255 characters!")]
        public string StudentName { get; set; }
        
        [Display(Name = "Class Name")]
        [Required(ErrorMessage = "Class Name is required!")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Name must be more than 3 characters and less than 255 characters!")]
        public string ClassName { get; set; }
        
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Name is required!")]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Name must be more than 3 characters and less than 255 characters!")]
        public string Title { get; set; }
        
        [Display(Name = "Date")]
        [Required(ErrorMessage = "Date is required!")]
        public DateTime LeaveDate { get; set; }= DateTime.Now;

        [Display(Name = "Leave Type")]
        [Required(ErrorMessage = "Name is required!")]
        public LeaveType LeaveType { get; set; }
        public string Reason { get; set; }
        public Status Status { get; set; }
    }
}
